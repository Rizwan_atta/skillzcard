package com.infosys.skillzCard

import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.View
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_get_fp.*
import org.json.JSONObject

@Suppress("DEPRECATION")
class GetFPActivity : AppCompatActivity(), View.OnClickListener {

        lateinit var email:String
        lateinit var token:String
         lateinit var  pDialog:ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_get_fp)

        email = intent.getStringExtra("user_email")

        pDialog = ProgressDialog(this)
        pDialog.setTitle("Please wait")
        pDialog.setMessage("work in progress")


        btnGFpSubmitCode.setOnClickListener(this)
        btnGFPCancel.setOnClickListener(this)


    }

    override fun onClick(v: View?) {
        when (v) {

            btnGFpSubmitCode->{
                pDialog.show()
                token = edtTxtGFPCode.text.toString()
                if(token.isEmpty()){
                    edtTxtGFPCode.error = "please enter your token"
                    pDialog.dismiss()
                }else{
                    validateTheFPToken(email,token)

                }

            }


            btnGFPCancel->{
                startActivity(Intent(applicationContext,LoginActivity::class.java))
                finish()
            }
        }
    }


    private fun validateTheFPToken(incomingUserEmail: String,incomingUserFpToken: String){
        val mStringRequest = object : StringRequest(Request.Method.POST, applicationContext.resources.getString(R.string.base_url)+"check/email/token", Response.Listener { s ->
            val mJsonObj = JSONObject(s)
            if(mJsonObj.getInt("status") == 1){
                pDialog.dismiss()
                val i = Intent(applicationContext,ResetPasswordActivity::class.java)
                i.putExtra("email",incomingUserEmail)
                i.putExtra("token",incomingUserFpToken)
                startActivity(i)
                finish()
            }else{
                pDialog.dismiss()
                showSnackBar("something went wrong retry please", 0)
            }



        }, Response.ErrorListener { e ->
            // Your error code here
            pDialog.dismiss()
            showSnackBar("Server side error please retry", 0)
        }) {
            override fun getParams(): Map<String, String> = mapOf(
                    "email" to incomingUserEmail,
                    "token" to incomingUserFpToken
            )
        }

        val mRequestQueue = Volley.newRequestQueue(applicationContext)
        mRequestQueue.add(mStringRequest)
    }

    //    ------------------Show Snack bars------------------------- //
    private fun showSnackBar(message: String, type: Int) {
        val mSnackBar: Snackbar = Snackbar.make(frameGFPContainer, message, Snackbar.LENGTH_LONG)
        when (type) {
            0 -> {
                mSnackBar.view.setBackgroundColor(applicationContext.resources.getColor(R.color.Red))
                mSnackBar.show()
            }
            1 -> {
                mSnackBar.view.setBackgroundColor(applicationContext.resources.getColor(R.color.green))
                mSnackBar.show()
            }
            else -> {
                mSnackBar.view.setBackgroundColor(applicationContext.resources.getColor(R.color.colorPrimary))
                mSnackBar.show()
            }
        }


    }
}
