package com.infosys.skillzCard

import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.View
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_reset_password.*
import org.json.JSONObject
@Suppress("DEPRECATION")
class ResetPasswordActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var newPass: String
    lateinit var ConfPass: String
        lateinit var  pDialog: ProgressDialog
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)
        btnRPLogin.setOnClickListener(this)
        btnRPCancel.setOnClickListener(this)

        pDialog = ProgressDialog(this)
        pDialog.setTitle("Please wait")
        pDialog.setMessage("work in progress")

    }

    override fun onClick(v: View?) {

        when (v) {
            btnRPLogin -> {
                newPass = edtTxtResetPass.text.toString()
                ConfPass = edtTxtConfResetPass.text.toString()

                if (newPass.isEmpty() && ConfPass.isEmpty()) {
                    edtTxtResetPass.error = "fill the field"
                    edtTxtConfResetPass.error = "fill the field"
                } else if (newPass.isEmpty()) {
                    edtTxtResetPass.error = "fill the field"
                } else if (ConfPass.isEmpty()) {
                    edtTxtConfResetPass.error = "fill the field"
                } else if (newPass.length < 6) {
                    edtTxtResetPass.error = "at least six in count"
                } else if (newPass != ConfPass) {
                    edtTxtResetPass.error = "Password do not match"
                    edtTxtConfResetPass.error = "Password do not match"
                } else {
                    btnRPLogin.isEnabled = false
                    pDialog.show()
                    ResetNewPassword(intent.getStringExtra("email"), intent.getStringExtra("token"), newPass)
                }
            }
            btnRPCancel -> {
                startActivity(Intent(applicationContext, LoginActivity::class.java))
                finish()
            }
        }

    }


    private fun ResetNewPassword(incomingUserEmail: String, incomingUserFpToken: String, incomingUserPassword: String) {
        val mStringRequest = object : StringRequest(Request.Method.POST, applicationContext.resources.getString(R.string.base_url) + "reset/password", Response.Listener { s ->
            val mJsonObj = JSONObject(s)
            if (mJsonObj.getInt("status") == 1) {
                val i = Intent(applicationContext, LoginActivity::class.java)
                startActivity(i)
                finish()
            } else {
                pDialog.dismiss()
                btnRPLogin.isEnabled = true
                showSnackBar("something went wrong retry please", 0)
            }


        }, Response.ErrorListener { e ->
            btnRPLogin.isEnabled = true
            pDialog.dismiss()
            // Your error code here
            showSnackBar("Server side error please retry", 0)
        }) {
            override fun getParams(): Map<String, String> = mapOf(
                    "email" to incomingUserEmail,
                    "token" to incomingUserFpToken,
                    "password" to incomingUserPassword
            )
        }

        val mRequestQueue = Volley.newRequestQueue(applicationContext)
        mRequestQueue.add(mStringRequest)
    }


    //    ------------------Show Snack bars------------------------- //
    private fun showSnackBar(message: String, type: Int) {
        val mSnackBar: Snackbar = Snackbar.make(frameRPContainer, message, Snackbar.LENGTH_LONG)
        when (type) {
            0 -> {
                mSnackBar.view.setBackgroundColor(applicationContext.resources.getColor(R.color.Red))
                mSnackBar.show()
            }
            1 -> {
                mSnackBar.view.setBackgroundColor(applicationContext.resources.getColor(R.color.green))
                mSnackBar.show()
            }
            else -> {
                mSnackBar.view.setBackgroundColor(applicationContext.resources.getColor(R.color.colorPrimary))
                mSnackBar.show()
            }
        }


    }
}
