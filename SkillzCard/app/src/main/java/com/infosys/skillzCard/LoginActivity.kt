package com.infosys.skillzCard

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.View
import android.view.animation.Animation
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject
import java.util.regex.Pattern

@Suppress("DEPRECATION")
class LoginActivity : AppCompatActivity(), View.OnClickListener,Animation.AnimationListener {
    override fun onAnimationRepeat(animation: Animation?) {

    }

    override fun onAnimationEnd(animation: Animation?) {


    }

    override fun onAnimationStart(animation: Animation?) {


//        showSnackBar("items"+ ,1)

        if(bgViewFlipper.displayedChild ==5 ||bgViewFlipper.displayedChild ==9 ||bgViewFlipper.displayedChild ==8||bgViewFlipper.displayedChild ==10 ){

            txtVuForgetPassword.setTextColor(this.resources.getColor(R.color.black))
//            txtVuForgetPassword.setText
            txt_vu_login_with.setTextColor(this.resources.getColor(R.color.black))
            txtVuNewMemeber.setTextColor(this.resources.getColor(R.color.black))
            txtVuSignUp.setTextColor(this.resources.getColor(R.color.black))


        }else{
            txtVuForgetPassword.setTextColor(this.resources.getColor(R.color.blue))
//            txtVuForgetPassword.setText
            txt_vu_login_with.setTextColor(this.resources.getColor(R.color.white))
            txtVuNewMemeber.setTextColor(this.resources.getColor(R.color.white))
            txtVuSignUp.setTextColor(this.resources.getColor(R.color.blue))
        }

    }


    lateinit var incomingUserEmail: String
    lateinit var incomingUserPassword: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        bgViewFlipper.startFlipping()
        bgViewFlipper.setInAnimation(this, R.anim.alpha_fading_in_bg)
        bgViewFlipper.setOutAnimation(this, R.anim.alpha_fading_out_bg)
        bgViewFlipper.inAnimation.setAnimationListener(this)


//                getInAnimation().setAnimationListener


        btnLogin.setOnClickListener(this)
        txtVuForgetPassword.setOnClickListener(this)
        txtVuSignUp.setOnClickListener(this)
        imgVuFbLogin.setOnClickListener(this)

    }

    override fun onClick(p0: View?) {

        when (p0) {

            btnLogin -> {
                btnLogin.isEnabled = false
                incomingUserEmail = edtTxtEmailLogin.text.toString()
                incomingUserPassword = edtTxtLoginPassword.text.toString()

                if (locallyVerifyCredentials(incomingUserEmail, incomingUserPassword)) {

                    if (isNetworkAvailable()) {
                        showSnackBar("wait logging in!!", 1)
                        checkEmailAvailable(incomingUserEmail, incomingUserPassword)
                    } else {
                        showSnackBar("No internet found", 0)
                        btnLogin.isEnabled = true
                    }
                } else {
                    showSnackBar("Wrong values provided recheck", 0)
                    btnLogin.isEnabled = true
                }
            }

            txtVuForgetPassword -> {
                startActivity(Intent(this, ForgotPasswordActivity::class.java))
                finish()
            }

            txtVuSignUp -> {

                startActivity(Intent(this, SignUpActivity::class.java))

            }

            imgVuFbLogin -> {

            }
        }
    }

    private fun locallyVerifyCredentials(incomingUserEmail: String, incomingUserPassword: String): Boolean {

        if (incomingUserEmail.isEmpty() && incomingUserPassword.isEmpty()) {
            edtTxtEmailLogin.error = "please fill the field"
            edtTxtLoginPassword.error = "please fill the field"
            return false
        } else if (incomingUserEmail.isEmpty()) {
            edtTxtEmailLogin.error = "please fill the field"
            return false
        } else if (incomingUserPassword.isEmpty()) {
            edtTxtLoginPassword.error = "please fill the field"
            return false
        } else if (!isEmailValid(incomingUserEmail)) {
            edtTxtEmailLogin.error = "Email is not valid"
            return false
        }

        return true
    }


    private fun checkEmailAvailable(incomingUserEmail: String, incomingUserPassword: String) {
        val mStringRequest = object : StringRequest(Request.Method.POST, applicationContext.resources.getString(R.string.base_url) + "check-email-availability", Response.Listener { s ->
            val mJsonObj = JSONObject(s)
            if (mJsonObj.getInt("status") == 1) {
                LogTheUserIn(incomingUserEmail, incomingUserPassword)
            } else {
                edtTxtEmailLogin.error = "not a registered email"
                btnLogin.isEnabled = true
            }


        }, Response.ErrorListener { e ->
            // Your error code here
            showSnackBar("Server side error please retry", 0)
        }) {
            override fun getParams(): Map<String, String> = mapOf(
                    "email" to incomingUserEmail
                    , "is_beta_user" to applicationContext.resources.getString(R.string.beta_user)
            )
        }

        val mRequestQueue = Volley.newRequestQueue(applicationContext)
        mRequestQueue.add(mStringRequest)


    }

    private fun LogTheUserIn(incomingUserEmail: String, incomingUserPass: String) {
        val mStringRequest = object : StringRequest(Request.Method.GET, applicationContext.resources.getString(R.string.base_url) + "login?email=" + incomingUserEmail + "&password=" + incomingUserPass, Response.Listener { s ->
            val mJsonObj = JSONObject(s)

            // Your error code here
            showSnackBar(mJsonObj.toString(), 1)


        }, Response.ErrorListener { e ->
            // Your error code here
            showSnackBar("Server side error please retry", 0)
        }) {}

        val mRequestQueue = Volley.newRequestQueue(applicationContext)
        mRequestQueue.add(mStringRequest)


    }


    //    ------------------INTERNET CONNECTIVITY METHODS------------------------- //

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null
    }

    //    ------------------Email Pattern checker------------------------- //

    private fun isEmailValid(email: String): Boolean {
        val regExpn = "^([a-zA-Z0-9_\\-.]+)@([a-zA-Z0-9_\\-.]+)\\.([a-zA-Z]{2,5})\$"

        val inputStr = email

        val pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(inputStr)

        return matcher.matches()
    }


    //    ------------------Show Snack bars------------------------- //
    private fun showSnackBar(message: String, type: Int) {
        val mSnackBar: Snackbar = Snackbar.make(frameRegContainer, message, Snackbar.LENGTH_LONG)
        when (type) {
            0 -> {
                mSnackBar.view.setBackgroundColor(applicationContext.resources.getColor(R.color.Red))
                mSnackBar.show()
            }
            1 -> {
                mSnackBar.view.setBackgroundColor(applicationContext.resources.getColor(R.color.green))
                mSnackBar.show()
            }
            else -> {
                mSnackBar.view.setBackgroundColor(applicationContext.resources.getColor(R.color.colorPrimary))
                mSnackBar.show()
            }
        }


    }


}
