package com.infosys.skillzCard

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.View
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_confirm_email.*
import org.json.JSONObject

@Suppress("DEPRECATION")
class ConfirmEmailActivity : AppCompatActivity() ,View.OnClickListener {

     lateinit var ValidCode:String
     lateinit var incomingUserEmail:String
     lateinit var incomingUserPass:String
    lateinit var  pDialog: ProgressDialog
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirm_email)
        btnConfSubmitCode.setOnClickListener(this)
        incomingUserEmail = intent.getStringExtra("user_email")
        incomingUserPass = intent.getStringExtra("user_password")
        pDialog = ProgressDialog(this)
        pDialog.setTitle("Please wait")
        pDialog.setMessage("work in progress")

    }

    override fun onClick(v: View?) {
        when (v){

            btnConfSubmitCode ->{
                ValidCode =  edtTxtFieldValidation.text.toString()
                if(!ValidCode.isEmpty()){
                    if(isNetworkAvailable()){
                        pDialog.show()
                        ValidateReg(incomingUserEmail,ValidCode)
                    }else{
                     showSnackBar("No internet found",0)
                    }

                }else{
                    edtTxtFieldValidation.error = "Fill this field"
                }

            }


        }

    }


    private fun ValidateReg(incomingUserEmail: String,incomingValidationPin:String) {
        val mStringRequest = object : StringRequest(Request.Method.POST, applicationContext.resources.getString(R.string.base_url)+"email/check", Response.Listener { s ->
            val mJsonObj = JSONObject(s)
            if(mJsonObj.getString("status") == "1"){
                pDialog.dismiss()
                startActivity(Intent(this,SelectUTypeActivity::class.java))

            }else{
                pDialog.dismiss()
              showSnackBar("Not a valid Token please recheck",0)
            }



        }, Response.ErrorListener { e ->
            // Your error code here
            pDialog.dismiss()
            showSnackBar("Server side error please retry", 0)
        }) {
            override fun getParams(): Map<String, String> = mapOf(
                    "email" to incomingUserEmail
                    ,"code" to incomingValidationPin
            )
        }

        val mRequestQueue = Volley.newRequestQueue(applicationContext)
        mRequestQueue.add(mStringRequest)


    }


    //    ------------------INTERNET CONNECTIVITY METHODS------------------------- //

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null
    }

    //    ------------------Show Snack bars------------------------- //
    private fun showSnackBar(message: String, type: Int) {
        val mSnackBar: Snackbar = Snackbar.make(frameRegContainer, message, Snackbar.LENGTH_LONG)
        when (type) {
            0 -> {
                mSnackBar.view.setBackgroundColor(applicationContext.resources.getColor(R.color.Red))
                mSnackBar.show()
            }
            1 -> {
                mSnackBar.view.setBackgroundColor(applicationContext.resources.getColor(R.color.green))
                mSnackBar.show()
            }
            else -> {
                mSnackBar.view.setBackgroundColor(applicationContext.resources.getColor(R.color.colorPrimary))
                mSnackBar.show()
            }
        }


    }





}
