package com.infosys.skillzCard

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.infosys.skillzCard.fragment_artist.ArtistRegisterActivity
import com.infosys.skillzCard.CompanyFragments.RegisterActivity
import kotlinx.android.synthetic.main.activity_select_utype.*

class SelectUTypeActivity : AppCompatActivity(), View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_utype)
        btnSetupProfileArtist.setOnClickListener(this)
        btnSetupProfileAgency.setOnClickListener(this)
        back_btn_setUpProfile.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {

            btnSetupProfileArtist -> {
                startActivity(Intent(this, ArtistRegisterActivity::class.java))
            }


            btnSetupProfileAgency -> {
                startActivity(Intent(this, RegisterActivity::class.java))
            }
            back_btn_setUpProfile -> {
             super.onBackPressed()
            }


        }
    }
}
