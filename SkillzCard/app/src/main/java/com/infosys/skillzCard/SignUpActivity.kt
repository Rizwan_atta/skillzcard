package com.infosys.skillzCard

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_sign_up.*
import org.json.JSONObject
import java.util.regex.Pattern

@Suppress("DEPRECATION")
class SignUpActivity : AppCompatActivity(), View.OnClickListener {


    lateinit var userEmail: String
    lateinit var userPassword: String
    lateinit var userConfPassword: String
    lateinit var userValidationCode: String



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        btnRegSignUp.setOnClickListener(this)

    }


    override fun onClick(v: View?) {

        when (v) {
            btnRegSignUp -> {

                userEmail = edtTxtRegEmail.text.toString()
                userPassword = edtTxtRegPassword.text.toString()
                userConfPassword = edtTxtRegConfPass.text.toString()
                userValidationCode = edtTxtRegzValidation.text.toString()

                btnRegSignUp.isEnabled = false

                if (validateLocally(userEmail, userPassword, userConfPassword, userValidationCode)) {

                     if (isNetworkAvailable()) {
                         lin_lay_sign_up_prog.visibility = View.VISIBLE
                         checkEmailAvailable(userEmail,userPassword)

                    }else{
                         btnRegSignUp.isEnabled = true
                         showSnackBar("network is not available",0)
                     }

                }else{
                    btnRegSignUp.isEnabled = true
                    Toast.makeText(this,"check field",Toast.LENGTH_LONG).show()
                }


            }
        }

    }

    private fun validateLocally(userEmail: String,
                                userPassword: String,
                                userConfPassword: String,
                                userValidationCode: String): Boolean {

         val isValid:Boolean

        if (userEmail.isEmpty()
                && userPassword.isEmpty()
                && userConfPassword.isEmpty()
                && userValidationCode.isEmpty()) {

            edtTxtRegEmail.error = "Fill this field"
            edtTxtRegPassword.error = "Fill this field"
            edtTxtRegConfPass.error = "Fill this field"
            edtTxtRegzValidation.error = "Fill this field"
            isValid = false
            return isValid

        } else if (userEmail.isEmpty()) {
            edtTxtRegEmail.error = "Fill this field"
            isValid = false
            return isValid

        } else if (userPassword.isEmpty()) {
            edtTxtRegPassword.error = "Fill this field"
            isValid = false
            return isValid

        } else if (userConfPassword.isEmpty()) {
            edtTxtRegConfPass.error = "Fill this field"
            isValid = false
            return isValid

        } else if (userValidationCode.isEmpty()) {
            edtTxtRegzValidation.error = "Fill this field"
            isValid = false
            return isValid

        } else if (!isEmailValid(userEmail)) {
            edtTxtRegEmail.error = "email is not valid"
            isValid = false
            return isValid

        } else if (userPassword.length < 6) {
            edtTxtRegPassword.error = "At least 6 characters"
            isValid = false
            return isValid

        } else if (userPassword != userConfPassword) {
            edtTxtRegPassword.error = "password is not similar"
            edtTxtRegConfPass.error = "password is not similar"
            isValid = false
            return isValid

        }else{
            return true
        }



    }



    private fun checkEmailAvailable(incomingUserEmail: String,incomingUserPassword:String) {
        val mStringRequest = object : StringRequest(Request.Method.POST, applicationContext.resources.getString(R.string.base_url)+"check-email-availability", Response.Listener { s ->
            val mJsonObj = JSONObject(s)
            if(mJsonObj.getInt("status") == 1){
                signUpProgressBar.visibility = View.GONE

                lin_lay_sign_up_prog.isEnabled = true
                val i  = Intent(this, ConfirmEmailActivity::class.java)
                i.putExtra("user_email",incomingUserEmail)
                i.putExtra("user_password",incomingUserPassword)
                startActivity(i)
                finish()
            }else{
                lin_lay_sign_up_prog.visibility = View.GONE
                edtTxtRegEmail.error = "not a registered email"
                showSnackBar("not a registered email", 0)
                btnRegSignUp.isEnabled = true
            }



        }, Response.ErrorListener { e ->
            // Your error code here
            lin_lay_sign_up_prog.visibility = View.GONE
            showSnackBar("Server side error please retry", 0)
            btnRegSignUp.isEnabled = true
        }) {
            override fun getParams(): Map<String, String> = mapOf(
                    "email" to incomingUserEmail
                    ,"is_beta_user" to applicationContext.resources.getString(R.string.beta_user)
            )
        }

        val mRequestQueue = Volley.newRequestQueue(applicationContext)
        mRequestQueue.add(mStringRequest)


    }


    //    ------------------INTERNET CONNECTIVITY METHODS------------------------- //

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null
    }

    //    ------------------Email Pattern checker------------------------- //

    private fun isEmailValid(email: String): Boolean {
        val regExpn = "^([a-zA-Z0-9_\\-.]+)@([a-zA-Z0-9_\\-.]+)\\.([a-zA-Z]{2,5})\$"

        val inputStr = email

        val pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(inputStr)

        return matcher.matches()
    }


    //    ------------------Show Snack bars------------------------- //
    private fun showSnackBar(message: String, type: Int) {
        val mSnackBar: Snackbar = Snackbar.make(frameRegContainer, message, Snackbar.LENGTH_LONG)
        when (type) {
            0 -> {
                mSnackBar.view.setBackgroundColor(applicationContext.resources.getColor(R.color.Red))
                mSnackBar.show()
            }
            1 -> {
                mSnackBar.view.setBackgroundColor(applicationContext.resources.getColor(R.color.green))
                mSnackBar.show()
            }
            else -> {
                mSnackBar.view.setBackgroundColor(applicationContext.resources.getColor(R.color.colorPrimary))
                mSnackBar.show()
            }
        }


    }

}
