package com.infosys.skillzCard

import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.util.Log
import android.view.View
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_forgot_password.*
import org.json.JSONObject
@Suppress("DEPRECATION")
class ForgotPasswordActivity : AppCompatActivity() ,View.OnClickListener{
    lateinit var  pDialog: ProgressDialog
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        btnFpSubmitCode.setOnClickListener(this)
        btnCancel.setOnClickListener(this)
        pDialog = ProgressDialog(this)
        pDialog.setTitle("Please wait")
        pDialog.setMessage("work in progress")
    }

    override fun onClick(v: View?) {

        when (v){

            btnFpSubmitCode->{
                pDialog.show()
               val incomingEmail =  edtTxtFPEmail.text.toString()
                btnFpSubmitCode.isEnabled = false

                if(incomingEmail.isEmpty()){
                    pDialog.dismiss()
                    edtTxtFPEmail.error = "please give your email"
                    btnFpSubmitCode.isEnabled = true
                }else{

                    checkEmailAvailibilty(incomingEmail)

                }
            }

            btnCancel->{
                startActivity(Intent(applicationContext,LoginActivity::class.java))
                finish()
            }
        }

    }


    private fun checkEmailAvailibilty(incomingUserEmail: String) {
        val mStringRequest = object : StringRequest(Request.Method.POST, applicationContext.resources.getString(R.string.base_url)+"check-email-availability", Response.Listener { s ->
            val mJsonObj = JSONObject(s)
            if(mJsonObj.getInt("status") == 1){
                btnFpSubmitCode.isEnabled = true
                forgetPasswordProcess(incomingUserEmail)

            }else{
                edtTxtFPEmail.error = "not a registered email"
                btnFpSubmitCode.isEnabled = true
                pDialog.dismiss()
            }



        }, Response.ErrorListener { e ->
            pDialog.dismiss()
            // Your error code here
            btnFpSubmitCode.isEnabled = true
            showSnackBar("Server side error please retry", 0)
        }) {
            override fun getParams(): Map<String, String> = mapOf(
                    "email" to incomingUserEmail
                    ,"is_beta_user" to applicationContext.resources.getString(R.string.beta_user)
            )
        }

        val mRequestQueue = Volley.newRequestQueue(applicationContext)
        mRequestQueue.add(mStringRequest)


    }

    private fun forgetPasswordProcess(incomingUserEmail: String){
        val mStringRequest = object : StringRequest(Request.Method.POST, applicationContext.resources.getString(R.string.base_url)+"forgot/password", Response.Listener { s ->
            val mJsonObj = JSONObject(s)
            if(mJsonObj.getInt("status") == 1){
                pDialog.dismiss()
                startActivity(Intent(applicationContext,GetFPActivity::class.java).putExtra("user_email",incomingUserEmail))
                finish()
            }else{
                pDialog.dismiss()
                showSnackBar("something went wrong retry please", 0)
                btnFpSubmitCode.isEnabled = true
            }



        }, Response.ErrorListener { e ->
            pDialog.dismiss()
            // Your error code here
            showSnackBar("Server side error please retry", 0)
        }) {
            override fun getParams(): Map<String, String> = mapOf(
                    "email" to incomingUserEmail
            )
        }

        val mRequestQueue = Volley.newRequestQueue(applicationContext)
        mRequestQueue.add(mStringRequest)
    }





    //    ------------------Show Snack bars------------------------- //
    private fun showSnackBar(message: String, type: Int) {
        val mSnackBar: Snackbar = Snackbar.make(frameFPContainer, message, Snackbar.LENGTH_LONG)
        when (type) {
            0 -> {
                mSnackBar.view.setBackgroundColor(applicationContext.resources.getColor(R.color.Red))
                mSnackBar.show()
            }
            1 -> {
                mSnackBar.view.setBackgroundColor(applicationContext.resources.getColor(R.color.green))
                mSnackBar.show()
            }
            else -> {
                mSnackBar.view.setBackgroundColor(applicationContext.resources.getColor(R.color.colorPrimary))
                mSnackBar.show()
            }
        }


    }


}
